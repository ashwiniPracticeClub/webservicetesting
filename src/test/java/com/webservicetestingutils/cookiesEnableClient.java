package com.webservicetestingutils;

import com.qmetry.qaf.automation.ws.rest.DefaultRestClient;
import com.sun.jersey.api.client.Client;

public class cookiesEnableClient extends DefaultRestClient{
	@Override
protected Client createClient() {
	Client client=super.createClient();
	client.getProperties().put("jersey.config.client.followredirects", true);
	client.addFilter(new SessionManagementFilter());
	return client;
	
}
}
