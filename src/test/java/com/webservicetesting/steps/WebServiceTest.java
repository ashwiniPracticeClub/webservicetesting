package com.webservicetesting.steps;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.Response;
import com.qmetry.qaf.automation.ws.rest.RestWSTestCase;
import com.sun.jersey.api.client.ClientResponse;
import com.webservicetestingutils.UserBean;


public class WebServiceTest extends RestWSTestCase {
	static String username;
	static String password;
	static String userID;
	static String mdSID;
	static String custId;
	String baseUrl = ConfigurationManager.getBundle().getString("env.baseurl");
	

	public static String getMdSID() {
		return mdSID;
	}

	public static String getCustId() {
		return custId;
	}

	public static void setCustId(String custId) {
		WebServiceTest.custId = custId;
	}

	public static void setMdSID(String mdSID) {
		WebServiceTest.mdSID = mdSID;
	}

	public static String getUserID() {
		return userID;
	}

	public static void setUserID(String userID) {
		WebServiceTest.userID = userID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@QAFTestStep(description = "User register using webservice")
	public void userRegisterUsingWebservice() {
		UserBean bean = new UserBean();
		bean.fillRandomData();
		setUsername(bean.getUsername());
		setPassword(bean.getPassword());
		ClientResponse resRegister =
				getClient().resource(baseUrl + "/register")
						.header("Content-Type", "application/json")
						.post(ClientResponse.class, new Gson().toJson(bean));
		Response response = new Response(resRegister);
		Reporter.log("Response for register user-------"+response.getMessageBody().toString());
		JSONObject obj=new JSONObject(response.getMessageBody().toString());
		System.out.println("test----------"+obj.getString("id"));
		setCustId(obj.getString("id"));
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(200));

	}

	@QAFTestStep(description = "User login with same credentials")
	public void userLoginWithSameCredentials() {
		ClientResponse resLogin = getClient()
				.resource(baseUrl + "/login").header("Content-Type", "application/json")
				.header("Authorization",
						"Basic " + getPassword(getUsername() + ":" + getPassword()))
						//"Basic " + getPassword("ashwini" + ":" + "user"))
				.get(ClientResponse.class);
		Reporter.log("Response for login user-------"+resLogin);
		Response response = new Response(resLogin);
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(200));
		setUserID(response.getCookies().get(0).toString().substring(10));

	}

	private String getPassword(String password) {
		Base64.Encoder encoder = Base64.getUrlEncoder();
		return encoder.encodeToString(password.getBytes());
	}

	@QAFTestStep(description = "User add item {0} in cart")
	public void userAddItemInCart(String item) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", item);
		ClientResponse resaddItem = getClient()
				.resource(baseUrl + "/cart").header("Content-Type", "application/json")
				.post(ClientResponse.class, new Gson().toJson(map));
		Response response = new Response(resaddItem);
		Reporter.log("Response for add item to cart-------"+response.getMessageBody());
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(201));
		
	}

	@QAFTestStep(description = "User add Shipping Address as {0},{1},{2},{3},{4}")
	public void userAddShippingAddressAs(String str0, String str1, String str2,
			String str3, String str4) {
		HashMap<String, Object> hashmap = new HashMap<String, Object>();
		hashmap.put("street", str0);
		hashmap.put("number", str1);
		hashmap.put("country", str2);
		hashmap.put("city", str3);
		hashmap.put("postcode", str4);
		hashmap.put("userID", getUserID());
		ClientResponse resaddAddress =
				getClient().resource(baseUrl + "/addresses")
						.header("Content-Type", "application/json")
						.post(ClientResponse.class, new Gson().toJson(hashmap));
		Reporter.log("Response for add adreess-------"+resaddAddress);
		Response response = new Response(resaddAddress);
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(200));
		ClientResponse resaddAddressGet =getClient().resource(baseUrl + "/addresses")
		.header("Content-Type", "application/json")
		.get(ClientResponse.class);
		Reporter.log("Response for get adreess-------"+resaddAddressGet);
		

	}

	@QAFTestStep(description = "User add card details as {0},{1},{2}")
	public void userAddCardDetailsAs(String str0, String str1, String str2) {
		HashMap<String, String> hashmap = new HashMap<String, String>();
		hashmap.put("longNum", str0);
		hashmap.put("expires", str1);
		hashmap.put("cvv", str2);
		hashmap.put("usreID", getUserID());
		ClientResponse resaddcards = getClient()
				.resource(baseUrl + "/cards").header("Content-Type", "application/json")
				.post(ClientResponse.class, new Gson().toJson(hashmap));
		Reporter.log("Response for add card details-------"+resaddcards);
		Response response = new Response(resaddcards);
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(200));
		
		ClientResponse resaddcardsGet = getClient()
				.resource(baseUrl + "/cards").header("Content-Type", "application/json")
				.get(ClientResponse.class);
		Reporter.log("Response for get card details-------"+resaddcardsGet);

	}

	@QAFTestStep(description = "User place order and verify the order")
	public void userPlaceOrderAndVerifyTheOrder() {
	getWebResource( "/orders").post();
	Reporter.log("Response for verify order-------"+getResponse().getMessageBody()+"status code"+getResponse().getStatus().getStatusCode());
	Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(201));
	
	
		ClientResponse resOrders = getClient()
				.resource(baseUrl + "/orders").header("Content-Type", "application/json")
				.get(ClientResponse.class);
		Response response = new Response(resOrders);
		Reporter.log("Response for verify order-------"+response.getMessageBody().toString());
		Validator.verifyThat(response.getMessageBody().toString(),
				Matchers.notNullValue());
	}

	@QAFTestStep(description = "User update item in cart quantity {0} for product {1}")
	public void userUpdateItemInCart(String str0,String str1) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("quantity", str0);
        map.put("id", str1);
        map.put("userID", getUserID());
        ClientResponse resUpdate = getClient()
				.resource(baseUrl + "/cart/update").header("Content-Type", "application/json")
				.post(ClientResponse.class, new Gson().toJson(map));
        Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(202));
		Reporter.log("Response for update cart-------"+resUpdate);
		

	}

	
	@QAFTestStep(description = "User verify cart")
	public void userVerifyCart() {
		ClientResponse resCart = getClient()
				.resource(baseUrl + "/cart").header("Content-Type", "application/json")
				.get(ClientResponse.class);
		Response response = new Response(resCart);
		Reporter.log("Response for verify cart-------"+response.getMessageBody().toString());
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(200));
	}

	
	@QAFTestStep(description = "User delete item {0} in cart")
	public void userDeleteItemInCart(String str0) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", str0);
		map.put("usreID", getUserID());
		ClientResponse resDelete = getClient()
				.resource(baseUrl + "/cart").header("Content-Type", "application/json")
				.delete(ClientResponse.class);
		Reporter.log("Response for delete item from cart-------"+resDelete);
		Response response = new Response(resDelete);
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(202));
	}

	@QAFTestStep(description = "User delete registered user")
	public void userDeleteRegisteredUser() {
	
		ClientResponse resDeleteUser = getClient()
				.resource(baseUrl + "/customers/"+getCustId()).header("Content-Type", "application/json")
				.delete(ClientResponse.class);
		Reporter.log("Response for delete registered user-------"+resDeleteUser);
		   Validator.verifyThat(getResponse().getStatus().getStatusCode(),Matchers.equalTo(200));
	}

}
